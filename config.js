Object.assign(process.env, {
    GIT_AUTHOR_NAME: 'Renovate Bot',
    GIT_AUTHOR_EMAIL: 'bot@example.com',
    GIT_COMMITTER_NAME: 'Renovate Bot',
    GIT_COMMITTER_EMAIL: 'bot@example.com',
  });
  
  module.exports = {
    endpoint: process.env.CI_API_V4_URL,
    hostRules: [
      {
        baseUrl: 'https://registry.example.com',
        username: 'other-user',
        password: process.env.GITLAB_REGISTRY_TOKEN,
      },
    ],
    platform: 'gitlab',
    username: 'renovate-bot',
    gitAuthor: 'Renovate Bot <bot@example.com>',
    autodiscover: true,
  };